var express=require("express");
var app=express();
var bodyParser=require("body-parser");
var mongoose=require("mongoose");
var Blogs=require("./models/blogs");
var User=require("./models/admin");
var Category=require("./models/category");
var flash=require("connect-flash");
var passport=require("passport");
var LocalStrategy=require("passport-local");
var slug = require("slug");
var methodOverride = require("method-override");
var blogsRoutes=require("./routes/blogs");
var adminRoutes=require("./routes/admin");
var authRoutes=require("./routes/index");
var slugRoutes=require("./routes/title");
mongoose.plugin(slug);
app.use(bodyParser.urlencoded({extended:true}));
app.use(methodOverride("_method"));
app.use(require("express-session")({
    secret:"once again rusty wins cutest dog",
    resave:false,
    saveUninitialized:false
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
mongoose.connect("mongodb://localhost/blogs",{useNewUrlParser:true,useUnifiedTopology:true});
app.set("view engine","ejs");
app.use(function(req,res,next){
    res.locals.isAuthenticated=req.isAuthenticated();
    next();
});
app.use(express.static(__dirname + "/public"));


app.use("/",authRoutes);
app.use("/title",slugRoutes);
app.use("/admin",adminRoutes);
app.use("/blogs",blogsRoutes);
app.get("*",function(req,res){
    res.render("error",{message:"ERROR!!, Could not find the match"});
});

app.listen(3000,function(){
    console.log("Blog App is working");
});