var express=require("express");
var router=express.Router();
var passport=require("passport");
var Blogs=require("../models/blogs");
var User=require("../models/admin");

router.get("/",function(req,res){
    res.redirect("/blogs");
});
router.get("/login",function(req,res){
    res.render("login");
});
router.post("/login",passport.authenticate("local",{
    successRedirect:"/admin",failureRedirect:"/login"
}),function(req,res){
    
});
router.get("/logout",function(req,res){
    req.logout();
    // res.locals.isAuthenticated=false;
    // req.session.destroy();
    return res.redirect("/");
});
module.exports=router;