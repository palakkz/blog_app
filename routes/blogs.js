var express=require("express");
var router=express.Router();
var Blogs=require("../models/blogs");
var User=require("../models/admin");
var Category=require("../models/category");

router.get("/",function(req,res){
 
    if(req.query.search){
        var message={};
        const regex = new RegExp(escapeRegex(req.query.search), 'gi');
        Blogs.find({title:regex},function(err,allblogs){
            if(err){
                console.log(err);
            }
            else{
                 if(allblogs.length<1){
                     res.redirect("*");
                 }
                 else
                {
                    // req.flash("success","Sorry could not find the match for blog");
                    res.render("home",{blogs:allblogs});
                }
            }
        });
    }
    else{
    
    Blogs.find({}).sort({created:-1}).exec(function(err,blogs){
        if(err){
            console.log(err);
        }
        else{
            
            res.render("home",{blogs:blogs});
        }
    });
}
});


router.get("/:id",function(req,res){
       Blogs.findById(req.params.id,function(err,foundBlog){
           if(err){

               console.log(err);

           }else{
          
            console.log(foundBlog);
            res.render("show",{blog: foundBlog});
           }
 
       });
});
function escapeRegex(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};

 module.exports=router;