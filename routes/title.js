var express=require("express");
var router=express.Router();
var Blogs=require("../models/blogs");
var User=require("../models/admin");
var Category=require("../models/category");
router.get("/:slug",function(req,res){
    Blogs.findOne({"slug":req.params.slug},function(err,foundblog){
         if(err){
            
             return res.render("home");
          }
          else{
            res.render("show",{blog:foundblog});
          }
      });
});
module.exports=router;