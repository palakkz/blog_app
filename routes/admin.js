var express=require("express");
var router=express.Router();
var Blogs=require("../models/blogs");
var User=require("../models/admin");
var Category=require("../models/category");
router.get("/",isLoggedIn,function(req,res){
    Blogs.find({}).sort({created:-1}).exec(function(err,foundblogs){
        if(err){
            console.log(err)
        }
        else{
            req.flash("info","welcome to admin page");
            res.render("adminpanel",{blogs:foundblogs,isAuthenticated:req.user});
        }
    });
   
});
router.get("/allblogs",isLoggedIn,function(req,res){
    
    Blogs.find({}).sort({created:-1}).exec(function(err,foundblogs){
        if(err){
            console.log(err);
        }
        else{
            res.render("allblogs",{blogs:foundblogs});
        }
     
    });
   
});
router.get("/allcategory",isLoggedIn,function(req,res){
    Category.find({},function(err,allcategory){
        if(err){
            console.log(err);
        }
        else
        {
            console.log(allcategory);
            res.render("category",{category:allcategory});
        }
    });
});
router.post("/allcategory",isLoggedIn,function(req,res){
    
    var name=req.body.category;
    var newCategory = {name:name};
    Category.create(newCategory,function(err,newlyCreated){
        if(err){
            console.log(err);
            res.render("new");
        }
        else{
            console.log(newlyCreated);
            res.redirect("/admin/allcategory");
        }
    });
   
});

router.get("/category/new",isLoggedIn,function(req,res){

    res.render("categoryNew");
});
router.get("/blogs/new",isLoggedIn,function(req,res){
    Category.find({},function(err,category){
        if(err){
            console.log(err);
        }
        else{
            console.log(category);
            res.render("new",{category:category});
        }
    });
});

router.post("/blogs",isLoggedIn,function(req,res){
    var title=req.body.blog.title;
    var slug =req.body.blog.slug;
    var category=req.body.blog.category;
    var image=req.body.blog.image;
    var content=req.body.blog.content;
    
    var newBlog = {title:title,slug:slug,category:category,image:image,content:content};
    
    Blogs.create(newBlog,function(err,newlyCreated){
        if(err){
            console.log(err);
            res.render("new");
        }else{
            console.log(newlyCreated);
            
                res.redirect("/admin/allblogs");

          
        }
    });
    req.params.id
});
router.get("/category/:id/edit",isLoggedIn,function(req,res){
    Category.findById(req.params.id,function(err,foundcategory){
        if(err){
            console.log(err);
        }
        else{
            res.render("edit_cat",{category:foundcategory})
        }
    });
});
router.put("/category/:id",isLoggedIn,function(req,res){
    Category.findByIdAndUpdate(req.params.id,req.body.category,function(err,updatedCategory){
        if(err){
            res.redirect("/admin");
        }
        else{
            res.redirect("/admin/allcategory");
        }
    });
});
router.delete("/category/:id",isLoggedIn,function(req,res){
    Category.findByIdAndDelete(req.params.id,function(err){
        if(err){
            res.redirect("/admin");
        }
        else{
            res.redirect("/admin/allcategory");
        }
    });

});
router.get("/blogs/:id/edit",isLoggedIn,function(req,res){
    Blogs.findById(req.params.id,function(err,foundBlog){
        if(err){
            console.log(err);
        }
        else{
            res.render("edit",{blog:foundBlog});
        }
    });
});
router.put("/blogs/:id",isLoggedIn,function(req,res){
    Blogs.findByIdAndUpdate(req.params.id,req.body.blog,function(err,updatedCampground){
        if(err){
            res.redirect("/");
        }else{
            res.redirect("/blogs/"+req.params.id);
        }
    });
});

router.delete("/blogs/:id",isLoggedIn,function(req,res){
    Blogs.findByIdAndRemove(req.params.id,function(err){
        if(err){
            res.redirect("/blogs");
        }
        else{
            res.redirect("/admin/allblogs");
        }
    });

});

function isLoggedIn(req,res,next){
    if(req.isAuthenticated()){
        return next();
    }
    req.flash("error","Please login first");
    res.redirect("/login");
};
module.exports=router;