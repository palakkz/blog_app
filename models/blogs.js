var mongoose=require("mongoose");
var slug = require("slug");
mongoose.plugin(slug);
var BlogSchema = new mongoose.Schema({
    title:String,
    slug:{type:String,slug:"title"},
    image: String,
    content:String,
    category:String,
    created:{type:Date,default:Date.now}
    
});
module.exports=mongoose.model("Blogs",BlogSchema);